package main

import (
	"context"
	"fmt"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

type Response events.APIGatewayProxyResponse

// Handler is our lambda handler invoked by the `lambda.Start` function call
func Handler(ctx context.Context) (Response, error) {
	conn, err := conn()
	if err != nil {
		return Response{StatusCode: 500, Body: err.Error()}, nil
	}

	val, err := incr(conn)
	if err != nil {
		return Response{StatusCode: 500, Body: err.Error()}, nil
	}

	resp := Response{
		StatusCode:      200,
		IsBase64Encoded: false,
		Body:            fmt.Sprintf("%+v", val),
	}

	return resp, nil
}

func incr(svc *dynamodb.DynamoDB) (*dynamodb.UpdateItemOutput, error) {
	input := &dynamodb.UpdateItemInput{
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":r": {
				N: aws.String("1"),
			},
		},
		TableName: aws.String("CountVisits"),
		Key: map[string]*dynamodb.AttributeValue{
			"page": {
				S: aws.String("my_site"),
			},
		},
		ReturnValues:     aws.String("UPDATED_NEW"),
		UpdateExpression: aws.String("set visits = visits + :r"),
	}

	u, err := svc.UpdateItem(input)

	if err != nil {
		return nil, err
	}

	return u, nil
}

func conn() (*dynamodb.DynamoDB, error) {
	sess, err := session.NewSession()

	if err != nil {
		return nil, err
	}

	// Create DynamoDB client
	svc := dynamodb.New(sess)

	return svc, nil
}

func main() {
	lambda.Start(Handler)
}
