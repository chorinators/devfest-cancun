# DevFest Cancún

Mi primer "Hola Mundo" en la nube usando:
- AWS
- Serverless
- Go

# Pre requisitos

- Crear una cuenta con AWS
    - Se necesita una tarjeta de débito/crédito con al menos US$ 1 de saldo
- Crear un IAM y descargar las credenciales
- Si no tienes un sistema operativo POSIX compliant:
    - Levantar una EC2 micro con Ubuntu 18.04

### Instalar:
- AWS SDK
    ```
    $ sudo apt install awscli
    ```
- Go
    ```
    $ sudo apt install golang-go
    ```
- npm
    ```
    $ sudo apt install npm
    ```
- Serverless
    ```
    $ sudo npm install -g serverless
    ```
- aws-lambda-go
    ```
    $ go get github.com/aws/aws-lambda-go/...
    $ go get github.com/aws/aws-lambda-go/lambda
    ```
## Crear el template usando Serverless
```
$ sls create -t aws-go -p devfest-cancun
```

## Deploy to AWS
```
$ aws configure
$ make deploy
```